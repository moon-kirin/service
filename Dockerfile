FROM quay.io/quarkus/quarkus-distroless-image:1.0

EXPOSE 8080

ENV quarkus.datasource.db-kind=postgresql \
    quarkus.datasource.jdbc.url= \
    quarkus.datasource.username=postgres \
    quarkus.datasource.password=postgres \
    quarkus.oidc.auth-server-url= \
    quarkus.rest-client.toranku-api.url= \
    quarkus.oidc-client.auth-server-url= \
    quarkus.oidc-client.client-id= \
    quarkus.oidc-client.credentials.secret=

COPY target/*-runner /application

USER nonroot

CMD ["./application", "-Dquarkus.http.host=0.0.0.0"]
