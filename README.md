# Kirin Project
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_service&metric=alert_status) ![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_service&metric=sqale_rating) ![Coverage](https://sonarcloud.io/api/project_badges/measure?project=moon-kirin_service&metric=coverage)](https://sonarcloud.io/summary/new_code?id=moon-kirin_service)

[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/reiizumi/kirin) ![Docker Image Size (latest semver)](https://img.shields.io/docker/image-size/reiizumi/kirin) ![Docker Pulls](https://img.shields.io/docker/pulls/reiizumi/kirin)](https://hub.docker.com/r/reiizumi/kirin)

## Introduction
This service allows you to manage the income, budgets and purchases made each year for a user.

You can find the information on how to use it according to the [OpenAPI](https://gitlab.com/moon-kirin/openapi/-/blob/master/reference/Kirin.yaml).

_URLs indicated in the OpenAPI file are from a private environment and are not accessible_

## Requirements
You can start the service locally to make the necessary modifications. The service is generated from [Quarkus](https://quarkus.io).

This app requires:
* Keycloak to authenticate users according to OIDC
* [Toranku](https://gitlab.com/moon-toranku) to manage the categories
* PostgreSQL database

### Keycloak configuration
Users require this role:
- kirin:user

Kirin also requires its own ClientId to connect to toranku according to the `user` role.

## Start locally
You can start the service locally to make the necessary modifications. The service is generated from [Quarkus](https://quarkus.io). 

### Update resources according to the OpenAPI
Resources and entities are generated automatically from the OpenAPI. These are executed automatically in the Maven process of test, compile or package (among others), but it can be called manually.

```shell script
./mvnw clean generate-sources
```

### Running the application in dev mode
To make code changes, start the code in development mode:
```shell script
./mvnw compile quarkus:dev
```
This mode uses the properties with the `%dev` prefix or those that do not have any prefix in the `resources/application.properties`.

### Creating a native executable
The compilation is done in native using Graal. No need to install Graal in your system, the compilation is done through a container. _This may take several minutes._

```shell script
./mvnw package -Pnative --define quarkus.native.container-build=true
```

You can then execute your native executable with: `./target/kirin-1.0.0-runner`

### Container
The service has been created to be packaged in a container with minimal libraries.

```shell script
docker build -f src/main/docker/dockerfile -t moon-cat/kirin .
```

You can find more container information in [Docker Hub](https://hub.docker.com/r/reiizumi/kirin)

## Helm
Helm chart is available in [Artifact Hub](https://artifacthub.io/packages/helm/kirin/kirin)

## Extra deploy documentation
More documentation (in Spanish) is found in the [Moon.cat](https://www.moon.cat/docs/k8s/services/kirin)

## Quality Check
The code is analyzed according to [SonarCloud](https://sonarcloud.io/project/overview?id=moon-kirin_service)

## Health status and metrics
* `/q/health/ready` indicates whether the service is available for use.
* `/q/metrics` metrics in Prometheus format.
