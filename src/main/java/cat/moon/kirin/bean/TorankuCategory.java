package cat.moon.kirin.bean;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnore;

@SuppressWarnings({"squid:S1104", "squid:S3252"})
public class TorankuCategory {
    public Long id;
    public String name;
    public String description;
    public String icon;
    @JsonIgnore
    public int sort;
    @JsonIgnore
    public List<String> tags;
}
