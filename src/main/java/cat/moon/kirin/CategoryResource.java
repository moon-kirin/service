package cat.moon.kirin;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.logging.Logger;
import javax.ws.rs.*;

import cat.moon.kirin.api.CategoriesApi;
import cat.moon.kirin.bean.TorankuCategory;
import cat.moon.kirin.mapper.CategoryMapper;
import cat.moon.kirin.model.Category;
import cat.moon.kirin.service.TorankuService;
import io.quarkus.oidc.client.Tokens;

@Path("/api/v1/categories")
public class CategoryResource {
    private static final Logger LOG = Logger.getLogger(CategoryResource.class);

    @Inject
    @RestClient
    TorankuService service;
    
    @Inject
    CategoryMapper mapper;

    @Inject Tokens tokens;

    @RolesAllowed("kirin:user") 
    @GET
    @Produces({ "application/json" }) 
    public Response getApiV1Categories() {
        List<TorankuCategory> torankuCategoryList = service.getCategories("Bearer "+tokens.getAccessToken());
        // List<Category> categories = mapper.toDtoList(torankuCategoryList);
        // LOG.info("Categories: " + torankuCategoryList.toString());
        return Response.ok(torankuCategoryList).build();
    }
}
