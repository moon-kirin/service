package cat.moon.kirin.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import cat.moon.kirin.model.FixedExpense.TypeEnum;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity(name="fixed_expenses")
@SuppressWarnings("squid:S1104")
public class FixedExpensesEntity extends PanacheEntity {
    @Column(length = 50)
    public String name;

    @Column(length = 100)
    public String description;
    public Integer categoryId;

    @Enumerated(EnumType.STRING)
    public TypeEnum type;
    public Double price;
    public Boolean requireMark;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "fixedexpense_id", referencedColumnName = "id", nullable = false, updatable = false)
    public List<FixedExpensesMonthEntity> months;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "year", referencedColumnName = "year")
    public PayrollEntity year;
}
