package cat.moon.kirin.entity;

import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity(name="fixed_expenses_months")
@SuppressWarnings("squid:S1104")
public class FixedExpensesMonthEntity extends PanacheEntity {
    public Integer month;
    public Boolean paid;
}
