package cat.moon.kirin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import cat.moon.kirin.model.Budget.TypeEnum;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity(name="budget")
@SuppressWarnings("squid:S1104")
public class BudgetEntity extends PanacheEntity {
    @Column(length = 50)
    public String name;
    public Double budget;

    @Enumerated(EnumType.STRING)
    public TypeEnum type;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "year", referencedColumnName = "year")
    public PayrollEntity year;
    
}
