package cat.moon.kirin.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity(name="purchases")
@SuppressWarnings("squid:S1104")
public class PurchaseEntity extends PanacheEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "budget_id", referencedColumnName = "id")
    public BudgetEntity budget;
    
    @Column(length = 50)
    public String name;

    @Column(length = 100)
    public String description;
    public Integer categoryId;

    public Double price;
    public Integer month;
}
