package cat.moon.kirin.entity.projection;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public class PayrollYear {
    public final Integer year;

    public PayrollYear(Integer year) {
        this.year = year;
    }
}
