package cat.moon.kirin.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

@Entity(name="payrollextra")
@SuppressWarnings("squid:S1104")
public class PayrollExtraEntity extends PanacheEntity {
    @Column(length = 50)
    public String name;
    public BigDecimal payroll;
}
