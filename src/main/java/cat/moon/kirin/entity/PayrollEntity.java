package cat.moon.kirin.entity;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;

@Entity(name="payroll")
@SuppressWarnings("squid:S1104")
public class PayrollEntity extends PanacheEntityBase  {
    @Id public Integer year;
    public BigDecimal payroll01;
    public BigDecimal payroll02;
    public BigDecimal payroll03;
    public BigDecimal payroll04;
    public BigDecimal payroll05;
    public BigDecimal payroll06;
    public BigDecimal payroll07;
    public BigDecimal payroll08;
    public BigDecimal payroll09;
    public BigDecimal payroll10;
    public BigDecimal payroll11;
    public BigDecimal payroll12;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.PERSIST)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "payrollYear", referencedColumnName = "year", nullable = false, updatable = false)
    public List<PayrollExtraEntity> extra;
}
