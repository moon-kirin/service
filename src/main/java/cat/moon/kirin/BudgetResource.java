package cat.moon.kirin;

import java.net.URI;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.kirin.api.BudgetsApi;
import cat.moon.kirin.entity.BudgetEntity;
import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.mapper.BudgetMapper;
import cat.moon.kirin.model.Budget;
import cat.moon.kirin.repository.BudgetRepository;
import cat.moon.kirin.repository.PayrollRepository;
import io.quarkus.panache.common.Sort;

public class BudgetResource implements BudgetsApi {
    @SuppressWarnings("squid:S1075")
    private static String basePath = "/api/v1/budgets/";

    @Inject
    BudgetRepository repository;

    @Inject
    PayrollRepository payrollRepository;

    @Inject
    BudgetMapper mapper;

    @Override
    @Transactional
    @RolesAllowed("kirin:user")
    public Response deleteV1BudgetsId(Integer id) {
        if(repository.deleteById(id.longValue())) {
            return Response.noContent().build();
        } else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1Budgets(@NotNull Integer year) {
        List<BudgetEntity> list = repository.find("year.year", Sort.descending("year"), year).list();
        return Response.ok(mapper.toDtoList(list)).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1BudgetsId(Integer id) {
        BudgetEntity entity = repository.findById(id.longValue());
        if(entity != null) return Response.ok(mapper.toDto(entity)).build();
        else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response postV1Budgets(@Valid Budget budget) {
        PayrollEntity exist = payrollRepository.findById(budget.getYear());
        if(exist != null) {
            BudgetEntity entity = mapper.toEntity(budget);
            entity.persist();
            return Response.created(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.CONFLICT).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response putV1BudgetsId(Integer id, @Valid Budget budget) {
        BudgetEntity entity = repository.findById(id.longValue());
        if(entity != null) {
            budget.setId(id);
            budget.setYear(entity.year.year);
            mapper.updateDto(entity, budget);
            return Response.noContent().location(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }
    
}
