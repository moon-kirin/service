package cat.moon.kirin;

import java.net.URI;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.kirin.api.FixedExpensesApi;
import cat.moon.kirin.entity.FixedExpensesEntity;
import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.mapper.FixedExpensesMapper;
import cat.moon.kirin.model.FixedExpense;
import cat.moon.kirin.repository.FixedExpensesRepository;
import cat.moon.kirin.repository.PayrollRepository;
import io.quarkus.panache.common.Sort;

public class FixedExpensesResource implements FixedExpensesApi {
    @SuppressWarnings("squid:S1075")
    private static String basePath = "/api/v1/fixed-expenses/";

    @Inject
    PayrollRepository payrollRepository;

    @Inject
    FixedExpensesRepository repository;

    @Inject
    FixedExpensesMapper mapper;

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response deleteV1FixedExpensesId(Integer id) {
        if(repository.deleteById(id.longValue())) {
            return Response.noContent().build();
        } else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1FixedExpenses(@NotNull Integer year) {
        List<FixedExpensesEntity> list = repository.find("year.year", Sort.descending("year"), year).list();
        return Response.ok(mapper.toDtoList(list)).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1FixedExpensesId(Integer id) {
        FixedExpensesEntity entity = repository.findById(id.longValue());
        if(entity != null) return Response.ok(mapper.toDto(entity)).build();
        else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response postV1FixedExpenses(@Valid FixedExpense fixedExpense) {
        PayrollEntity exist = payrollRepository.findById(fixedExpense.getYear());
        if(exist != null) {
            FixedExpensesEntity entity = mapper.toEntity(fixedExpense);
            entity.persist();
            return Response.created(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.CONFLICT).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response putV1FixedExpensesId(Integer id, @Valid FixedExpense fixedExpense) {
        FixedExpensesEntity entity = repository.findById(id.longValue());
        if(entity != null) {
            fixedExpense.setId(id);
            fixedExpense.setYear(entity.year.year);
            entity.months.clear();
            mapper.updateDto(entity, fixedExpense);
            return Response.noContent().location(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }
}
