package cat.moon.kirin.repository;

import javax.enterprise.context.ApplicationScoped;

import cat.moon.kirin.entity.BudgetEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class BudgetRepository implements PanacheRepository<BudgetEntity> {

}
