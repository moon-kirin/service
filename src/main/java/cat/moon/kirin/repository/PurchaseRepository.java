package cat.moon.kirin.repository;

import javax.enterprise.context.ApplicationScoped;

import cat.moon.kirin.entity.PurchaseEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class PurchaseRepository implements PanacheRepository<PurchaseEntity> {
    
}
