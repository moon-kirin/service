package cat.moon.kirin.repository;

import javax.enterprise.context.ApplicationScoped;

import cat.moon.kirin.entity.FixedExpensesEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class FixedExpensesRepository implements PanacheRepository<FixedExpensesEntity> {
    
}
