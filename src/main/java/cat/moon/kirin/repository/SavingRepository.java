package cat.moon.kirin.repository;

import javax.enterprise.context.ApplicationScoped;

import cat.moon.kirin.entity.SavingEntity;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

@ApplicationScoped
public class SavingRepository implements PanacheRepository<SavingEntity> {
    
}
