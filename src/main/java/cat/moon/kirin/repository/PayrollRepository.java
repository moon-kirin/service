package cat.moon.kirin.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.entity.projection.PayrollYear;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;
import io.quarkus.panache.common.Sort;

@ApplicationScoped
public class PayrollRepository implements PanacheRepositoryBase<PayrollEntity, Integer> {
    public void saveOrUpdate(Integer year, PayrollEntity entity) {
        if(entity == null) entity = new PayrollEntity();
        entity.year = year;
        if(!entity.isPersistent()) entity.persist();
    }

    public List<PayrollYear> getAllYears() {
        return findAll(Sort.by("year")).project(PayrollYear.class).list();
    }
}
