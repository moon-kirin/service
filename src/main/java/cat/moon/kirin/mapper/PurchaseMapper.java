package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.PurchaseEntity;
import cat.moon.kirin.model.Purchase;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PurchaseMapper {
    @Mapping(target = "budget.id", source = "budgetId")
    PurchaseEntity toEntity(Purchase purchase);

    @Mapping(target = "budgetId", source = "budget.id")
    Purchase toDto(PurchaseEntity purchaseEntity);

    @Mapping(target = "budget.id", source = "budgetId")
    void updateDto(@MappingTarget PurchaseEntity purchaseEntity, Purchase purchase);

    List<PurchaseEntity> toEntityList(List<Purchase> budgetList);

    List<Purchase> toDtoList(List<PurchaseEntity> purchaseEntityList);
}
