package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.BudgetEntity;
import cat.moon.kirin.model.Budget;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface BudgetMapper {
    @Mapping(target = "year.year", source = "year")
    BudgetEntity toEntity(Budget budget);

    @Mapping(target = "year", source = "year.year")
    Budget toDto(BudgetEntity budgetEntity);

    @Mapping(target = "year.year", source = "year")
    void updateDto(@MappingTarget BudgetEntity budgetEntity, Budget budget);

    List<BudgetEntity> toEntityList(List<Budget> budgetList);

    List<Budget> toDtoList(List<BudgetEntity> budgetEntityList);
}
