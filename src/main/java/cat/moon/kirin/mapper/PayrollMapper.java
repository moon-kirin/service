package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.model.Payroll;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PayrollMapper {
    PayrollEntity toEntity(Payroll payroll);

    Payroll toDto(PayrollEntity payrollEntity);

    void updateDto(@MappingTarget PayrollEntity payrollEntity, Payroll payroll);

    List<PayrollEntity> toEntityList(List<Payroll> payrollList);

    List<Payroll> toDtoList(List<PayrollEntity> payrollEntityList);
}
