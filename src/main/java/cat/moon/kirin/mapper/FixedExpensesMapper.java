package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.FixedExpensesEntity;
import cat.moon.kirin.model.FixedExpense;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface FixedExpensesMapper {
    @Mapping(target = "year.year", source = "year")
    FixedExpensesEntity toEntity(FixedExpense budget);

    @Mapping(target = "year", source = "year.year")
    FixedExpense toDto(FixedExpensesEntity budgetEntity);

    @Mapping(target = "year.year", source = "year")
    void updateDto(@MappingTarget FixedExpensesEntity budgetEntity, FixedExpense budget);

    List<FixedExpensesEntity> toEntityList(List<FixedExpense> budgetList);

    List<FixedExpense> toDtoList(List<FixedExpensesEntity> budgetEntityList);
}
