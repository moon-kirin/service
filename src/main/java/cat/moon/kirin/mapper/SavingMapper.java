package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.SavingEntity;
import cat.moon.kirin.model.Saving;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SavingMapper {
    @Mapping(target = "year.year", source = "year")
    SavingEntity toEntity(Saving item);

    @Mapping(target = "year", source = "year.year")
    Saving toDto(SavingEntity entity);

    @Mapping(target = "year.year", source = "year")
    void updateDto(@MappingTarget SavingEntity entity, Saving item);

    List<SavingEntity> toEntityList(List<Saving> list);

    List<Saving> toDtoList(List<SavingEntity> list);
}
