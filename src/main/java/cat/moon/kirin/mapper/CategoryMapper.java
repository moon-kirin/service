package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.bean.TorankuCategory;
import cat.moon.kirin.model.Category;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CategoryMapper {
    
    Category toDto(TorankuCategory torankuCategory);

    List<Category> toDtoList(List<TorankuCategory> torankuCategoryList);
}
