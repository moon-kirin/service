package cat.moon.kirin.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

import cat.moon.kirin.entity.projection.PayrollYear;
import cat.moon.kirin.model.Year;

@Mapper(componentModel = "cdi", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PayrollYearMapper {
    Year toDto(PayrollYear payrollYear);

    List<Year> toDtoList(List<PayrollYear> payrollYearList);
}
