package cat.moon.kirin.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import cat.moon.kirin.bean.TorankuCategory;

@Path("/api/v1")
@RegisterRestClient(configKey="toranku-api")
@ApplicationScoped
public interface TorankuService {
    @GET
    @Path("/categories")
    List<TorankuCategory> getCategories(@HeaderParam("Authorization") String bearer);
}
