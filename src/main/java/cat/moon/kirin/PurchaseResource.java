package cat.moon.kirin;

import java.net.URI;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.kirin.api.PurchaseApi;
import cat.moon.kirin.entity.BudgetEntity;
import cat.moon.kirin.entity.PurchaseEntity;
import cat.moon.kirin.mapper.PurchaseMapper;
import cat.moon.kirin.model.Purchase;
import cat.moon.kirin.repository.BudgetRepository;
import cat.moon.kirin.repository.PurchaseRepository;
import io.quarkus.logging.Log;
import io.quarkus.panache.common.Sort;

public class PurchaseResource implements PurchaseApi {
    @SuppressWarnings("squid:S1075")
    private static String basePath = "/api/v1/purchases/";

    @Inject
    BudgetRepository budgetRepository;

    @Inject
    PurchaseRepository repository;

    @Inject
    PurchaseMapper mapper;

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response deleteV1PurchasesId(Integer id) {
        if(repository.deleteById(id.longValue())) {
            return Response.noContent().build();
        } else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1Purchases(@NotNull Integer budgetId) {
        List<PurchaseEntity> list = repository.find("budget.id", Sort.descending("name"), budgetId.longValue()).list();
        return Response.ok(mapper.toDtoList(list)).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1PurchasesId(Integer id) {
        PurchaseEntity entity = repository.findById(id.longValue());
        if(entity != null) {
            Log.info("Id: "+entity.id);
            Log.info("BudgetId: "+entity.budget.id);
            return Response.ok(mapper.toDto(entity)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response postV1Purchases(@Valid Purchase purchase) {
        BudgetEntity exist = budgetRepository.findById(purchase.getBudgetId().longValue());
        if(exist != null) {
            PurchaseEntity entity = mapper.toEntity(purchase);
            entity.persist();
            return Response.created(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.CONFLICT).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response putV1PurchasesId(Integer id, @Valid Purchase purchase) {
        PurchaseEntity entity = repository.findById(id.longValue());
        if(entity != null) {
            purchase.setId(id);
            mapper.updateDto(entity, purchase);
            return Response.noContent().location(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }
    
}
