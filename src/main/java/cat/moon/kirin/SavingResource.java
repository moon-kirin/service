package cat.moon.kirin;

import java.net.URI;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.kirin.api.SavingApi;
import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.entity.SavingEntity;
import cat.moon.kirin.mapper.SavingMapper;
import cat.moon.kirin.model.Saving;
import cat.moon.kirin.repository.PayrollRepository;
import cat.moon.kirin.repository.SavingRepository;
import io.quarkus.panache.common.Sort;

public class SavingResource implements SavingApi {
    @SuppressWarnings("squid:S1075")
    private static String basePath = "/api/v1/savings/";

    @Inject
    SavingRepository repository;

    @Inject
    PayrollRepository payrollRepository;

    @Inject
    SavingMapper mapper;

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response deleteV1SavingsId(Integer id) {
        if(repository.deleteById(id.longValue())) {
            return Response.noContent().build();
        } else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1Savings(@NotNull Integer year) {
        List<SavingEntity> list = repository.find("year.year", Sort.descending("year"), year).list();
        return Response.ok(mapper.toDtoList(list)).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1SavingsId(Integer id) {
        SavingEntity entity = repository.findById(id.longValue());
        if(entity != null) return Response.ok(mapper.toDto(entity)).build();
        else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response postV1Savings(@Valid Saving saving) {
        PayrollEntity exist = payrollRepository.findById(saving.getYear());
        if(exist != null) {
            SavingEntity entity = mapper.toEntity(saving);
            entity.persist();
            return Response.created(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.CONFLICT).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response putV1SavingsId(Integer id, @Valid Saving saving) {
        SavingEntity entity = repository.findById(id.longValue());
        if(entity != null) {
            saving.setId(id);
            saving.setYear(entity.year.year);
            mapper.updateDto(entity, saving);
            return Response.noContent().location(URI.create(basePath+entity.id)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }
}
