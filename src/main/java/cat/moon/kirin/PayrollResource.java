package cat.moon.kirin;

import java.net.URI;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cat.moon.kirin.api.PayrollApi;
import cat.moon.kirin.entity.PayrollEntity;
import cat.moon.kirin.mapper.PayrollMapper;
import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.repository.PayrollRepository;

public class PayrollResource implements PayrollApi {
    @SuppressWarnings("squid:S1075")
    private static String basePath = "/api/v1/payrolls/";
    
    @Inject
    PayrollRepository repository;

    @Inject
    PayrollMapper mapper;

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response deleteV1PayrollsYear(Integer year) {
        if(repository.deleteById(year)) {
            return Response.noContent().build();
        } else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1Payrolls() {
        return Response.ok(repository.getAllYears()).build();
    }

    @Override
    @RolesAllowed("kirin:user")  
    public Response getV1PayrollsYear(Integer year) {
        PayrollEntity entity = repository.findById(year);
        if(entity != null) return Response.ok(mapper.toDto(entity)).build();
        else return Response.status(Status.NOT_FOUND).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response postV1PayrollsYear(Integer year, @Valid Payroll payroll) {
        PayrollEntity entity = repository.findById(year);
        if(entity == null) {
            repository.saveOrUpdate(year, mapper.toEntity(payroll));
            return Response.created(URI.create(basePath+year)).build();
        }
        else return Response.status(Status.CONFLICT).location(URI.create(basePath+year)).build();
    }

    @Override
    @Transactional
    @RolesAllowed("kirin:user")  
    public Response putV1PayrollsYear(Integer year, @Valid Payroll payroll) {
        PayrollEntity entity = repository.findById(year);
        if(entity != null) {
            entity.extra.clear();
            mapper.updateDto(entity, payroll);
            repository.saveOrUpdate(year, entity);
            return Response.noContent().location(URI.create(basePath+year)).build();
        }
        else return Response.status(Status.NOT_FOUND).build();
    }
}
