package cat.moon.kirin;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.model.Saving;
import cat.moon.kirin.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Create payroll
- Order 3: Others
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class SavingResourceTest {
    @Inject
    ItemUtil util;

    @Test
    @Order(99)
    void clean() {
        Log.info("Test - Clean Category");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, util.savingYear).delete(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
    }

    @Test
    @Order(2)
    @SuppressWarnings("squid:S2699")
    void createPayroll() {
        Payroll item = util.buildPayroll();
        util.createPayroll(util.savingYear, item);
    }

    @Test
    @Order(3)
    void getV1AllSavings() {
        Log.info("Test - Get all Savings");
        Saving item = util.buildSaving(util.savingYear);
        item.setId(util.createSaving(item));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlParamYear, util.savingYear).get(util.urlSavings)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        List<Saving> categories = response.body().jsonPath().getList(".", Saving.class);
        assertThat(categories, is(not(empty())));

        Optional<Saving> resp = categories.stream().filter(p -> p.getId().equals(item.getId())).findFirst();
        assertThat(resp, isPresent());
    }

    @Test
    @Order(1)
    void getV1AllSavingsEmpty() {
        Log.info("Test - Get all Savings");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlParamYear, 1000).get(util.urlSavings)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }
    
    @Test
    @Order(3)
    void getV1SavingsYear200() {
        Log.info("Test - Get Saving");
        Saving item = util.buildSaving(util.savingYear);
        item.setId(util.createSaving(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Saving p = response.as(Saving.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(1)
    void getV1SavingsYear404() {
        Log.info("Test - Get Saving - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 6000).get(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void postV1SavingsYear201() {
        Log.info("Test - Create Saving");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildSaving(util.savingYear))
            .when().post(util.urlSavings)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), is(util.urlSavingsMatcher));
    }

    @Test
    @Order(3)
    void postV1SavingsYear409() {
        Log.info("Test - Create Saving - Conflict");

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildSaving(1000))
            .when().post(util.urlSavings)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
    }

    @Test
    @Order(1)
    void postV1SavingsYear400() {
        Log.info("Test - Create Saving - Bad Request");
        Saving item = util.buildSaving(util.savingYear);
        item.setAmount(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(util.urlSavings)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(1));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=postV1Savings.saving.amount")
            ));
        }
    }

    @Test
    @Order(3)
    void putV1SavingsYear204() {
        Log.info("Test - Update Saving");
        Saving item = util.buildSaving(util.savingYear);
        item.setId(util.createSaving(item));
        item.setName("TestSaving");
        item.setAmount(new BigDecimal("5000.11"));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Saving p = response.as(Saving.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(3)
    void putV1SavingsYear400() {
        Log.info("Test - Update Saving - Bad request");
        Saving item = util.buildSaving(util.savingYear);
        item.setId(util.createSaving(item));
        item.setName(null);
        item.setAmount(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(2));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=putV1SavingsId.saving.name"),
                containsString("path=putV1SavingsId.saving.amount")
            ));
        }
    }

    @Test
    @Order(1)
    void putV1SavingsYear404() {
        Log.info("Test - Update Saving - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildSaving(util.savingYear))
            .when().pathParam(util.urlParamId, 5000).put(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void deleteV1SavingsYear200() {
        Log.info("Test - Delete Saving");
        Saving item = util.buildSaving(util.savingYear);
        item.setId(util.createSaving(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).delete(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1SavingsYear404() {
        Log.info("Test - Delete Saving - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 5000).delete(util.urlSavingsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
