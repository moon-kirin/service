// package cat.moon.kirin;

// import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
// import static org.hamcrest.MatcherAssert.assertThat;
// import static org.hamcrest.Matchers.empty;
// import static org.hamcrest.Matchers.is;
// import static org.hamcrest.Matchers.not;

// import io.quarkus.logging.Log;
// import io.quarkus.oidc.client.Tokens;
// import io.quarkus.test.common.QuarkusTestResource;
// import io.quarkus.test.junit.QuarkusTest;
// import io.quarkus.test.junit.mockito.InjectMock;
// import io.quarkus.test.oidc.server.OidcWiremockTestResource;
// import io.restassured.RestAssured;
// import io.restassured.response.Response;

// import org.eclipse.microprofile.rest.client.inject.RestClient;
// import org.junit.jupiter.api.Test;
// import org.mockito.Mockito;

// import cat.moon.kirin.bean.TorankuCategory;
// import cat.moon.kirin.model.Category;
// import cat.moon.kirin.service.TorankuService;
// import cat.moon.kirin.util.AccessTokenUtil;
// import cat.moon.kirin.util.ItemUtil;

// import java.util.Arrays;
// import java.util.HashSet;
// import java.util.List;
// import java.util.Optional;

// import javax.inject.Inject;

// @QuarkusTest
// @QuarkusTestResource(OidcWiremockTestResource.class)
// class CategoryResourceTest {
//     private static final String url = "/api/v1/categories";
//     private static final String torankuToken = "torankuFakeToken";

//     private static final String token = AccessTokenUtil.get("fakeuser", new HashSet<>(Arrays.asList("kirin:user")));
    
//     @Inject
//     ItemUtil util;
    
//     @InjectMock
//     @RestClient
//     TorankuService service;

//     @InjectMock
//     Tokens tokens;

//     @Test
//     void getV1Categories( ) {
//         Log.info("Test - Get Categories");

//         TorankuCategory item = util.buildTorankuCategory();
//         List<TorankuCategory> torankuCategories = Arrays.asList(item);

//         Mockito.when(tokens.getAccessToken()).thenReturn(torankuToken);
//         Mockito.when(service.getCategories("Bearer "+torankuToken)).thenReturn(torankuCategories);

//         Response response = RestAssured.given()
//             .auth().oauth2(token)
//             .when().get(url)
//             .then().extract().response();
//         assertThat(response.statusCode(), is(200));
//         List<Category> categories = response.body().jsonPath().getList(".", Category.class);
//         assertThat(categories, is(not(empty())));

//         Optional<Category> responseCategory = categories.stream().filter(p -> Long.valueOf(p.getId()).equals(item.id)).findFirst();
//         assertThat(responseCategory, isPresent());
//         util.compare(responseCategory.get(), item);
//     }
// }
