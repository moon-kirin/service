package cat.moon.kirin;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.kirin.model.Budget;
import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.model.Purchase;
import cat.moon.kirin.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty and 404
- Order 2: Create purchase, 400
- Order 3: Others
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class PurchaseResourceTest {
    private static Integer budgetId;

    @Inject
    ItemUtil util;

    @Test
    @Order(99)
    void clean() {
        Log.info("Test - Clean Budget");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, budgetId).delete(util.urlBudgetsById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, util.purchasesYear).delete(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
    }

    @Test
    @Order(2)
    @SuppressWarnings("squid:S2699")
    void createBudget() {
        Payroll p = util.buildPayroll();
        util.createPayroll(util.purchasesYear, p);
        Budget item = util.buildBudget(util.purchasesYear);
        budgetId = util.createBudget(item);
    }

    @Test
    @Order(3)
    void getV1AllPurchases() {
        Log.info("Test - Get all Purchases");
        Purchase item = util.buildPurchase(budgetId);
        item.setId(util.createPurchase(item));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlQueryBudgetId, budgetId).get(util.urlPurchases)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        List<Purchase> categories = response.body().jsonPath().getList(".", Purchase.class);
        assertThat(categories, is(not(empty())));

        Optional<Purchase> resp = categories.stream().filter(p -> p.getId().equals(item.getId())).findFirst();
        assertThat(resp, isPresent());
    }

    @Test
    @Order(1)
    void getV1AllPurchasesEmpty() {
        Log.info("Test - Get all Purchases");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlQueryBudgetId, 1000).get(util.urlPurchases)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }
    
    @Test
    @Order(3)
    void getV1PurchasesYear200() {
        Log.info("Test - Get Purchase");
        Purchase item = util.buildPurchase(budgetId);
        item.setId(util.createPurchase(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Purchase p = response.as(Purchase.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(1)
    void getV1PurchasesYear404() {
        Log.info("Test - Get Purchase - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 6000).get(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void postV1PurchasesYear201() {
        Log.info("Test - Create Purchase");
        Purchase cosa4 = util.buildPurchase(budgetId);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildPurchase(budgetId))
            .when().post(util.urlPurchases)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), is(util.urlPurchasesMatcher));
    }

    @Test
    @Order(3)
    void postV1PurchasesYear409() {
        Log.info("Test - Create Purchase - Conflict");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildPurchase(1000))
            .when().post(util.urlPurchases)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
    }

    @Test
    @Order(3)
    void postV1PurchasesYear400() {
        Log.info("Test - Create Purchase - Bad Request");
        Purchase item = util.buildPurchase(budgetId);
        item.setName(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(util.urlPurchases)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(1));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=postV1Purchases.purchase.name")
            ));
        }
    }

    @Test
    @Order(3)
    void putV1PurchasesYear204() {
        Log.info("Test - Update Purchase");
        Purchase item = util.buildPurchase(budgetId);
        item.setId(util.createPurchase(item));
        item.setName("TestPurchase");
        item.setPrice(new BigDecimal("5000.11"));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Purchase p = response.as(Purchase.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(3)
    void putV1PurchasesYear400() {
        Log.info("Test - Update Purchase - Bad request");
        Purchase item = util.buildPurchase(budgetId);
        item.setId(util.createPurchase(item));
        item.setName(null);
        item.setPrice(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(2));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=putV1PurchasesId.purchase.name"),
                containsString("path=putV1PurchasesId.purchase.price")
            ));
        }
    }

    @Test
    @Order(3)
    void putV1PurchasesYear404() {
        Log.info("Test - Update Purchase - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildPurchase(budgetId))
            .when().pathParam(util.urlParamId, 5000).put(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void deleteV1PurchasesYear200() {
        Log.info("Test - Delete Purchase");
        Purchase item = util.buildPurchase(budgetId);
        item.setId(util.createPurchase(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).delete(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1PurchasesYear404() {
        Log.info("Test - Delete Purchase - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 5000).delete(util.urlPurchasesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
