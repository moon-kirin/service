package cat.moon.kirin;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.kirin.model.FixedExpense;
import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Create payroll
- Order 3: Others
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class FixedExpensesResourceTest {
    @Inject
    ItemUtil util;

    @Test
    @Order(99)
    void clean() {
        Log.info("Test - Clean Payroll");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, util.fixedExpenseYear).delete(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));
    }

    @Test
    @Order(2)
    @SuppressWarnings("squid:S2699")
    void createPayroll() {
        Payroll item = util.buildPayroll();
        util.createPayroll(util.fixedExpenseYear, item);
    }

    @Test
    @Order(3)
    void getV1AllFixedExpenses() {
        Log.info("Test - Get all FixedExpenses");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setId(util.createFixedExpenses(item));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlParamYear, util.fixedExpenseYear).get(util.urlFixedExpenses)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        List<FixedExpense> categories = response.body().jsonPath().getList(".", FixedExpense.class);
        assertThat(categories, is(not(empty())));

        Optional<FixedExpense> resp = categories.stream().filter(p -> p.getId().equals(item.getId())).findFirst();
        assertThat(resp, isPresent());
    }

    @Test
    @Order(1)
    void getV1AllFixedExpensesEmpty() {
        Log.info("Test - Get all FixedExpenses");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().queryParam(util.urlParamYear, 1000).get(util.urlFixedExpenses)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }
    
    @Test
    @Order(3)
    void getV1FixedExpensesYear200() {
        Log.info("Test - Get FixedExpense");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setId(util.createFixedExpenses(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        FixedExpense p = response.as(FixedExpense.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(1)
    void getV1FixedExpensesYear404() {
        Log.info("Test - Get FixedExpense - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 6000).get(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void postV1FixedExpensesYear201() {
        Log.info("Test - Create FixedExpense");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildFixedExpense(util.fixedExpenseYear))
            .when().post(util.urlFixedExpenses)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), is(util.urlFixedExpensesMatcher));
    }

    @Test
    @Order(3)
    void postV1FixedExpensesYear409() {
        Log.info("Test - Create FixedExpense - Conflict");

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildFixedExpense(1000))
            .when().post(util.urlFixedExpenses)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
    }

    @Test
    @Order(1)
    void postV1FixedExpensesYear400() {
        Log.info("Test - Create FixedExpense - Bad Request");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setName(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(util.urlFixedExpenses)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(1));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=postV1FixedExpenses.fixedExpense.name")
            ));
        }
    }

    @Test
    @Order(3)
    void putV1FixedExpensesYear204() {
        Log.info("Test - Update FixedExpense");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setId(util.createFixedExpenses(item));
        item.setName("TestFixedExpense");
        item.setPrice(new BigDecimal("5000.11"));

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        FixedExpense p = response.as(FixedExpense.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(3)
    void putV1FixedExpensesYear400() {
        Log.info("Test - Update FixedExpense - Bad request");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setId(util.createFixedExpenses(item));
        item.setName(null);
        item.setPrice(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamId, item.getId()).put(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(2));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=putV1FixedExpensesId.fixedExpense.name"),
                containsString("path=putV1FixedExpensesId.fixedExpense.price")
            ));
        }
    }

    @Test
    @Order(1)
    void putV1FixedExpensesYear404() {
        Log.info("Test - Update FixedExpense - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildFixedExpense(util.fixedExpenseYear))
            .when().pathParam(util.urlParamId, 5000).put(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(3)
    void deleteV1FixedExpensesYear200() {
        Log.info("Test - Delete FixedExpense");
        FixedExpense item = util.buildFixedExpense(util.fixedExpenseYear);
        item.setId(util.createFixedExpenses(item));
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).delete(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, item.getId()).get(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1FixedExpensesYear404() {
        Log.info("Test - Delete FixedExpense - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamId, 5000).delete(util.urlFixedExpensesById)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
