package cat.moon.kirin.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

import javax.enterprise.context.ApplicationScoped;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import cat.moon.kirin.bean.TorankuCategory;
import cat.moon.kirin.model.Budget;
import cat.moon.kirin.model.Category;
import cat.moon.kirin.model.FixedExpense;
import cat.moon.kirin.model.FixedExpenseMonth;
import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.model.PayrollExtra;
import cat.moon.kirin.model.Purchase;
import cat.moon.kirin.model.Saving;
import cat.moon.kirin.model.Budget.TypeEnum;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

@ApplicationScoped
public class ItemUtil {
    public final String fakeUser = "fakeUser";
    public final String token = AccessTokenUtil.get(fakeUser, new HashSet<>(Arrays.asList("kirin:user")));
    
    public final String urlParamYear = "year";
    public final String urlParamId = "id";
    public final String urlQueryYear = "year";
    public final String urlQueryBudgetId = "budgetId";

    public final String urlPayroll = "/api/v1/payrolls";
    public final String urlPayrollByYear = urlPayroll+"/{"+urlParamYear+"}";
    
    public final String urlSavings = "/api/v1/savings";
    public final String urlSavingsById = urlSavings+"/{"+urlParamId+"}";
    
    public final String urlBudgets = "/api/v1/budgets";
    public final String urlBudgetsById = urlBudgets+"/{"+urlParamId+"}";

    public final String urlFixedExpenses = "/api/v1/fixed-expenses";
    public final String urlFixedExpensesById = urlFixedExpenses+"/{"+urlParamId+"}";

    public final String urlPurchases = "/api/v1/purchases";
    public final String urlPurchasesById = urlPurchases+"/{"+urlParamId+"}";

    public final String idMatcher = "[\\d]+$";
    public final Matcher<String> urlPayrollMatcher = Matchers.matchesPattern("http[s]?:\\/\\/[a-zA-Z\\d:.@]+\\/api\\/v1\\/payrolls\\/"+idMatcher);
    public final Matcher<String> urlSavingsMatcher = Matchers.matchesPattern("http[s]?:\\/\\/[a-zA-Z\\d:.@]+\\/api\\/v1\\/savings\\/"+idMatcher);
    public final Matcher<String> urlBudgetsMatcher = Matchers.matchesPattern("http[s]?:\\/\\/[a-zA-Z\\d:.@]+\\/api\\/v1\\/budgets\\/"+idMatcher);
    public final Matcher<String> urlFixedExpensesMatcher = Matchers.matchesPattern("http[s]?:\\/\\/[a-zA-Z\\d:.@]+\\/api\\/v1\\/fixed-expenses\\/"+idMatcher);
    public final Matcher<String> urlPurchasesMatcher = Matchers.matchesPattern("http[s]?:\\/\\/[a-zA-Z\\d:.@]+\\/api\\/v1\\/purchases\\/"+idMatcher);
    
    private int year = 2000;
    public int savingYear = 3000;
    public int budgetYear = 3001;
    public int fixedExpenseYear = 3002;
    public int purchasesYear = 3002;

    public void createPayroll(Integer year, Payroll item) {
        if(year != null) item.setYear(year);
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(urlParamYear, item.getYear()).post(urlPayrollByYear)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlPayrollMatcher);
    }

    public Integer createSaving(Saving item) {
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(urlSavings)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlSavingsMatcher);
        return retrieveId(location);
    }

    public Integer createBudget(Budget item) {
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(urlBudgets)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlBudgetsMatcher);
        return retrieveId(location);
    }

    public Integer createFixedExpenses(FixedExpense item) {
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(urlFixedExpenses)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlFixedExpensesMatcher);
        return retrieveId(location);
    }

    public Integer createPurchase(Purchase item) {
        Response response = RestAssured.given()
            .auth().oauth2(token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().post(urlPurchases)
            .then().extract().response();
        String location = response.header("Location");
        assertThat(response.statusCode(), is(201));
        assertThat(location, urlPurchasesMatcher);
        return retrieveId(location);
    }

    public Payroll buildPayroll() {
        Payroll p = new Payroll();
        p.setPayroll01(new BigDecimal("1000.00"));
        p.setPayroll02(new BigDecimal("1000.00"));
        p.setPayroll03(new BigDecimal("1000.00"));
        p.setPayroll04(new BigDecimal("1000.00"));
        p.setPayroll05(new BigDecimal("1000.00"));
        p.setPayroll06(new BigDecimal("1000.00"));
        p.setPayroll07(new BigDecimal("1000.00"));
        p.setPayroll08(new BigDecimal("1000.23"));
        p.setPayroll09(new BigDecimal("1000.00"));
        p.setPayroll10(new BigDecimal("1000.00"));
        p.setPayroll11(new BigDecimal("1000.00"));
        p.setPayroll12(new BigDecimal("1000.00"));
        PayrollExtra e = new PayrollExtra();
        e.setName("Christmas");
        e.setPayroll(new BigDecimal("200.99"));
        p.setExtra(Arrays.asList(e));
        return p;
    }

    public Saving buildSaving(Integer year) {
        Saving item = new Saving();
        item.setName("Holidays");
        item.setAmount(new BigDecimal("1000.21"));
        item.setYear(year);
        return item;
    }
    
    public Budget buildBudget(Integer year) {
        Budget item = new Budget();
        item.setName("Home");
        item.setType(TypeEnum.MONTHLY);
        item.setBudget(new BigDecimal("200.53"));
        item.setYear(year);
        return item;
    }
    
    public FixedExpense buildFixedExpense(Integer year) {
        FixedExpense item = new FixedExpense();
        item.setName("Netflix");
        item.setDescription("Netflix monthly subscription");
        item.setCategoryId(5);
        item.setType(FixedExpense.TypeEnum.UNITARY);
        item.setPrice(new BigDecimal("12.34"));
        item.setRequireMark(true);
        FixedExpenseMonth month1 = new FixedExpenseMonth();
        month1.setMonth(11);
        month1.setPaid(true);
        List<FixedExpenseMonth> months = Arrays.asList(month1);
        item.setMonths(months);
        item.setYear(year);
        return item;
    }

    public Purchase buildPurchase(Integer budgetId) {
        Purchase item = new Purchase();
        item.setName("Netflix");
        item.setDescription("Netflix monthly subscription");
        item.setCategoryId(5);
        item.setPrice(new BigDecimal("12.34"));
        item.setMonth(11);
        item.setBudgetId(budgetId);
        return item;
    }

    public TorankuCategory buildTorankuCategory() {
        TorankuCategory c = new TorankuCategory();
        c.id = 1L;
        c.name = "house";
        c.description = "house tools";
        c.icon = "fa-home";
        c.sort = 5;
        c.tags = Arrays.asList("house", "home");
        return c;
    }

    public void compare(Category c, TorankuCategory item) {
        assertThat(Long.valueOf(c.getId()), is(item.id));
        assertThat(c.getName(), is(item.name));
        assertThat(c.getDescription(), is(item.description));
        assertThat(c.getIcon(), is(item.icon));
    }
    
    public Integer retrieveId(String location) {
        Pattern p = Pattern.compile(idMatcher);
        java.util.regex.Matcher m = p.matcher(location);
        assertTrue(m.find());
        Integer id = Integer.parseInt(m.group());
        assertNotNull(id);
        return id;
    }

    public int nextYear() {
        return year++;
    }
}
