package cat.moon.kirin.util;

import java.util.Set;

import io.smallrye.jwt.build.Jwt;

public class AccessTokenUtil {
    public static String get(String userName, Set<String> groups) {
        return Jwt.preferredUserName(userName)
                .groups(groups)
                .issuer("https://server.example.com")
                .audience("https://service.example.com")
                .sign();
    }
}
