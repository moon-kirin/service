package cat.moon.kirin;

import static com.github.npathai.hamcrestopt.OptionalMatchers.isPresent;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import cat.moon.kirin.model.Payroll;
import cat.moon.kirin.model.PayrollExtra;
import cat.moon.kirin.model.Year;
import cat.moon.kirin.util.ItemUtil;
import io.quarkus.logging.Log;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.oidc.server.OidcWiremockTestResource;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

/*
- Order 1: Empty, 400 and 404
- Order 2: Others
*/
@QuarkusTest
@TestMethodOrder(OrderAnnotation.class)
@QuarkusTestResource(OidcWiremockTestResource.class)
class PayrollResourceTest {
    @Inject
    ItemUtil util;

    @Test
    @Order(2)
    void getV1AllPayrolls() {
        Log.info("Test - Get all payrolls");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlPayroll)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        List<Year> categories = response.body().jsonPath().getList(".", Year.class);
        assertThat(categories, is(not(empty())));

        Optional<Year> resp = categories.stream().filter(p -> p.getYear().equals(item.getYear())).findFirst();
        assertThat(resp, isPresent());
    }

    @Test
    @Order(1)
    void getV1AllPayrollsEmpty() {
        Log.info("Test - Get all payrolls - Empty");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().get(util.urlPayroll)
            .then().extract().response();
        
        assertThat(response.statusCode(), is(200));
        assertThat(response.jsonPath().getString("."), is("[]"));
    }
    
    @Test
    @Order(2)
    void getV1Payroll200() {
        Log.info("Test - Get Payroll");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, item.getYear()).get(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Payroll p = response.as(Payroll.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(1)
    void getV1Payroll404() {
        Log.info("Test - Get Payroll  - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, 1000).get(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(2)
    void postV1Payroll201() {
        Log.info("Test - Create Payroll");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildPayroll())
            .when().pathParam(util.urlParamYear, 2001).post(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(201));
        assertThat(response.header("Location"), is(util.urlPayrollMatcher));
    }

    @Test
    @Order(2)
    void postV1PayrollsYear409() {
        Log.info("Test - Create Payroll - 409");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamYear, item.getYear()).post(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(409));
        assertThat(response.header("Location"), is(util.urlPayrollMatcher));
    }

    @Test
    @Order(1)
    void postV1PayrollsYear400() {
        Log.info("Test - Create Payroll - Bad request");
        Payroll p = util.buildPayroll();
        p.setPayroll07(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(p)
            .when().pathParam(util.urlParamYear, 1000).post(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(1));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=postV1PayrollsYear.payroll.payroll07")
            ));
        }
    }

    @Test
    @Order(2)
    void putV1PayrollsYear204() {
        Log.info("Test - Update Payroll");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);

        item.setPayroll01(new BigDecimal("5000.11"));
        PayrollExtra extra = new PayrollExtra();
        extra.setName("TestExtra");
        extra.setPayroll(new BigDecimal("5001.52"));
        List<PayrollExtra> extraList = Arrays.asList(extra);
        item.setExtra(extraList);

        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamYear, item.getYear()).put(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, item.getYear()).get(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(200));
        Payroll p = response.as(Payroll.class);
        assertThat(p, is(item));
    }

    @Test
    @Order(1)
    void putV1PayrollsYear400() {
        Log.info("Test - Update Payroll - Bad request");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);
        item.setPayroll01(null);
        item.getExtra().get(0).setName(null);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(item)
            .when().pathParam(util.urlParamYear, item.getYear()).put(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(400));
        assertFalse(response.jsonPath().getList("parameterViolations").isEmpty());
        assertThat(response.jsonPath().getList("parameterViolations").size(), is(2));
        List<Map<Integer, Map<String, String>>> violations = response.jsonPath().getList("parameterViolations");
        for (Map<Integer, Map<String, String>> object : violations) {
            assertThat(object.toString(), anyOf(
                containsString("path=putV1PayrollsYear.payroll.payroll01"),
                containsString("path=putV1PayrollsYear.payroll.extra[0].name")
            ));
        }
    }

    @Test
    @Order(2)
    void putV1PayrollsYear404() {
        Log.info("Test - Update Payroll - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .contentType(ContentType.JSON)
            .body(util.buildPayroll())
            .when().pathParam(util.urlParamYear, util.nextYear()).put(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(2)
    void deleteV1PayrollsYear200() {
        Log.info("Test - Delete Payroll");
        Payroll item = util.buildPayroll();
        util.createPayroll(util.nextYear(), item);
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, item.getYear()).delete(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(204));

        response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, item.getYear()).get(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }

    @Test
    @Order(1)
    void deleteV1PayrollsYear404() {
        Log.info("Test - Delete Payroll - Not found");
        Response response = RestAssured.given()
            .auth().oauth2(util.token)
            .when().pathParam(util.urlParamYear, 1000).delete(util.urlPayrollByYear)
            .then().extract().response();
        assertThat(response.statusCode(), is(404));
    }
}
